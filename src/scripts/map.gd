extends Node2D

var map_tile_scene = preload("res://src/scenes/map_tile.tscn")
var apple_scene = preload("res://src/scenes/apple.tscn")

onready var snake = get_node("snake")

var tiles = []
var apple


func _ready():
	_make_map()
	_set_snake()
	_make_apple()


func _make_map():
	var map_size = _get_map_size()
	var tile_positions = _get_tile_positions(map_size)
	_make_all_tiles(tile_positions)
	


func _get_tile_positions(map_size):
	var start_postion = Vector2(0,0)
	var tile_size = _get_tile_size()
	map_size = map_size / tile_size
	start_postion.x +=  (fmod(map_size.x, 1.0) / 2) + (tile_size.x)
	start_postion.y +=  (fmod(map_size.y, 1.0) / 2) + (tile_size.y / 1.4)
	
	var tile_positions = []
	for x in map_size.x - 1:
		for y in map_size.y - 1:
			var x_pos = x * tile_size.x
			var y_pos = y * tile_size.y
			tile_positions.append(start_postion + Vector2(x_pos, y_pos))
			
	return tile_positions


func _get_map_size():
	var screen_size = OS.get_window_size() - Vector2(20,20)
	return screen_size


func _make_all_tiles(tile_positions):
	for pos in tile_positions:
		_make_tile(pos)


func _make_tile(tile_pos):
	var tile = map_tile_scene.instance()
	tile.position = tile_pos
	tiles.append(tile)
	add_child(tile)


func _get_tile_size():
	var tile = map_tile_scene.instance()
	tile._ready()
	return tile.get_size()


func _set_snake():
	var center_tile = tiles[tiles.size()/2]
	snake.set_initial_position(center_tile.position)
	snake.head.connect("eating_apple", self, "_on_apple_eaten")
	snake.head.connect("died", self, "_on_snake_died")


func _on_apple_eaten():
	apple.queue_free()
	_make_apple()


func _on_snake_died():
	var _discrard = get_tree().reload_current_scene()


func _make_apple():
	randomize()
	var random_index = rand_range(0, tiles.size() - 1)
	while not _can_place_apple_on_tile(tiles[random_index]):
		random_index = rand_range(0, tiles.size() - 1)
	
	apple = apple_scene.instance()
	var random_tile = tiles[random_index]
	random_tile.call_deferred("add_child", apple)


func _can_place_apple_on_tile(tile):
	if tile.get_overlapping_areas():
		return false
	return true
