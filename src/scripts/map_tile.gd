extends Area2D

onready var sprite = get_node("sprite")


func get_size():
	return sprite.texture.get_size() * sprite.scale
