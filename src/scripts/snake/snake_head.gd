extends Area2D

signal moved(pre, curent)
signal died
signal eating_apple

onready var sprite = get_node("sprite")
onready var timer = get_node("timer")

var standard_timer_time
var fast_timer_time = 0.15
var size
var move_direction = Vector2(1, 0)

func _ready():
	standard_timer_time = timer.wait_time
	size = get_size()


func get_size():
	return sprite.texture.get_size() * sprite.scale


func _on_timer_timeout():
	_move()


func _move():
	var pre_pos = position
	position += move_direction * size
	
	emit_signal("moved", pre_pos, position)
	
	yield(get_parent().get_parent().get_tree().create_timer(0.2), "timeout")
	if not _is_on_tile():
		die()


func _is_on_tile():
	var areas = get_overlapping_areas()
	for area in areas:
		if area.is_in_group("tiles"):
			return true
	return false


func _input(event):
	if event.is_action_pressed("snake_up"):
		move_direction = Vector2(0,-1)
	if event.is_action_pressed("snake_down"):
		move_direction = Vector2(0,1)
	if event.is_action_pressed("snake_right"):
		move_direction = Vector2(1,0)
	if event.is_action_pressed("snake_left"):
		move_direction = Vector2(-1,0)
	
	if event.is_action_pressed("speed_up"):
		_move()
		timer.wait_time = fast_timer_time
	if event.is_action_released("speed_up"):
		timer.wait_time = standard_timer_time
	


func die():
	print("dead")
	emit_signal("died")


func _on_snake_head_area_entered(area):
	if area in _get_body_pieces():
		die()
		
	if area.is_in_group("apple"):
		emit_signal("eating_apple")


func _get_body_pieces():
	return get_parent().body.pieces
