extends Node2D

export(int) var body_number = 4

onready var body = get_node("snake_body")
onready var head = get_node("snake_head")
var piece_size

func _ready():
	piece_size = head.get_size()
	z_index = 10
	_add_initial_body_pieces()


func _add_initial_body_pieces():
	for i in range(body_number):
		var head_of_set = Vector2(piece_size.x * -(i + 2), -piece_size.y)
		body.add_body_piece(head.position + (piece_size + head_of_set))


func set_initial_position(position):
	self.position = position
