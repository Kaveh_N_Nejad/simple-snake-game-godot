extends Node2D

var body_piece_scene = preload("res://src/scenes/snake/snake_body_piece.tscn")

var pieces = []
var positions = []
var size = 0
var need_to_add_piece = false

func add_piece():
	pass


func add_body_piece(position):
	var piece = body_piece_scene.instance()
	piece.position = position
	pieces.append(piece)
	positions.append(position)
	add_child(piece)
	size += 1


func _on_snake_head_moved(head_pre_pos, _head_pos):
	follow(head_pre_pos)


func follow(head_pre_pos):
	var end_piece_pos = positions.back()
		
	for i in range(pieces.size() - 1, 0, - 1):
		move_piece(positions[i - 1], i)
	move_piece(head_pre_pos, 0)
	
	if need_to_add_piece:
		add_body_piece(end_piece_pos)
		need_to_add_piece = false


func move_piece(new_pos, piece_index):
	pieces[piece_index].position = new_pos
	positions[piece_index] = new_pos


func _on_snake_head_eating_apple():
	need_to_add_piece = true
