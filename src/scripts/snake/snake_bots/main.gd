extends Node

onready var less_than_25_percent = load("res://src/scripts/snake/snake_bots/less_than_25_percent.gd").new()

var curent_script

var snake
var map
var body_positions
var map_positions
var apple_pos
var head_pos

func _ready():
	less_than_25_percent.main_script = self
	curent_script = less_than_25_percent


func _get_direction():
	curent_script.get_next_direction()


func _set_variable():
	body_positions = snake.get_body_positions()
	map_positions = main_script.map_positions
	apple_pos = main_script.apple_pos
	head_pos = snake.get_head_pos()
